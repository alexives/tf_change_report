provider "gitlab" {
    version = "~> 2.5.0"
}

provider "aws" {
  region = "us-east-2"
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket = "alexives-tf-backend-merge-request-report"
    key = "global/s3/terraform.tfstate"
    region = "us-east-2"
    encrypt = true
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "alexives-tf-backend-merge-request-report"
  lifecycle {
    prevent_destroy = true
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "gitlab_project" "report_example" {
    name = "Report tf changes"
    path = "tf_change_report"
    request_access_enabled = false
    shared_runners_enabled = true
    default_branch = "master"
    visibility_level = "public"
    description = "An example of how to show plan reports in merge requests"
}
